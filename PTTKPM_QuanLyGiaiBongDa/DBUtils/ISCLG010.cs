﻿/*
 * 1642021 - Ha Nguyen Thai Hoc
 */
using Models;

namespace DBUtils
{
    public interface ISCLG010
    {
        NguoiDungModel GetUser(params object[] paramArr);
    }
}
