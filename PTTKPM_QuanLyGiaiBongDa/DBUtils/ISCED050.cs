﻿using Models;
using System.Collections.ObjectModel;

namespace DBUtils
{
	public interface ISCED050
	{
		//Lay Danh sach doi bong
		ObservableCollection<DoiBongModel> GetDanhSachDoiBong(params object[] paramArr);
		//Update thong tin san van dong
		bool UpdateSanVanDong(params object[] paramArr);
		//Update thong tin san van dong
		bool DeleteSanVanDong(params object[] paramArr);
		//Them san van dong
		bool AddSanVanDong(params object[] paramArr);
		//Lay so san van dong lon nhat
		string CountSan(params object[] paramArr);
	}
}
