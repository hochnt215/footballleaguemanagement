﻿using Models;
using System.Collections.ObjectModel;

namespace DBUtils
{
	public interface ISCED070
	{
		//Lay danh sach bang cap
		ObservableCollection<BangCapModel> GetListBangCap(params object[] paramArr);
		//Update thong tin trong tai
		bool UpdateTrongTai(params object[] paramArr);
		//Update thong tin trong tai
		bool DeleteTrongTai(params object[] paramArr);
		//Them trong tai
		bool AddTrongTai(params object[] paramArr);
		//Dem so luong trong tai
		string CountTrongTai(params object[] paramArr);
	}
}
