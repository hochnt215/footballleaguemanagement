﻿using Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBUtils
{
	public interface ISCRC080
	{
		//Load quy định
		QuyDinhModel LoadQuyDinh(params object[] paramArr);
		//Load loại bàn thắng
		ObservableCollection<LoaiBanThangModel> LoadLoaiBanThang(params object[] paramArr);
		string CountMaxLoaiBT(params object[] paramArr);
		bool AddLoaiBanThang(params object[] paramArr);
		bool UpdateLoaiBanThang(params object[] paramArr);
		bool DeleteLoaiBanThang(params object[] paramArr);
		bool UpdateQD1(params object[] paramArr);
		bool UpdateQD3(params object[] paramArr);
		bool UpdateQD4(params object[] paramArr);
	}
}
