﻿using Models;
using System.Collections.ObjectModel;

namespace DBUtils
{
    public interface ISCED010
    {
        //Lay danh sach nguoi dung
        ObservableCollection<NguoiDungModel> GetListNguoiDung(params object[] paramArr);

        //Lay danh sach loai NV
        ObservableCollection<LoaiNVModel> GetListLoaiNV(params object[] paramArr);

        //Lấy mã NV lớn nhất
        int CountNV(params object[] paramArr);

        //Cấp tài khoản mới
        bool CapTaiKhoan(params object[] paramArr);

        //Sửa tài khoản
        bool SuaTaiKhoan(params object[] paramArr);

        //Xóa tài khoản
        bool XoaTaiKhoan(params object[] paramArr);
    }
}
