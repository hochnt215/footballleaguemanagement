﻿using Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBUtils
{
	public interface ISCRC050
	{
		//Lay danh sach lich thi dau
		ObservableCollection<TranDauModel> GetLichThiDau(params object[] paramArr);

		//Update thong tin tran dau
		bool UpdateTranDau(params object[] paramArr);

		//Lay trong tai bat tran dau
		ObservableCollection<TrongTaiModel> GetLstTrongTai(params object[] paramArr);

		//Tao lich thi dau
		bool TaoLichThiDau(params object[] paramArr);
	}
}
