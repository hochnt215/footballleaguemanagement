﻿/*
 * 1642021 - Ha Nguyen Thai Hoc
 * 13/03/2017 - BaseModel
 */
using Microsoft.TeamFoundation.MVVM;
using System.ComponentModel;
using System.Data;

namespace Utils
{

    public class BaseModel : ViewModelBase, IDataErrorInfo
    {
        #region IDataErrorInfo Members

        public string Error
        {
            get
            {
                return string.Empty;
            }
        }
        public string this[string columnName]
        {
            get
            {
                return string.Empty;
            }
        }

        public int ErrorCount
        {
            get
            {
                return 0;
            }
        }

        #endregion

        /// <summary>
        /// Map fields in IDataReader object to current object's properties
        /// </summary>
        /// <param name="reader"></param>
        /// <remarks></remarks>
        public virtual void DataMap(IDataReader reader)
        {

        }

        public bool ColumnExists(IDataReader reader, string columnName)
        {
            for (int i = 0; i <= reader.FieldCount - 1; i++)
            {
                if ((reader.GetName(i) == columnName))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
