﻿/*
 * 1642021 - Ha Nguyen Thai Hoc
 */

using Models;
using System;
using Utils;

namespace QuanLyGiaiBongDa.ViewModels
{
    partial class SCMN010VM_MainWindow : BaseViewModel
    {
        //private static readonly ILog Log = LogManager.GetLogger(typeof(SCMN010VM_MainWindow));
       

        #region Constructor
        public SCMN010VM_MainWindow(params object[] args)
            : base(args)
        {
            
        }
        #endregion

        #region Override
        protected override void InitializeProperties()
        {
            //Log.Info("InitializeProperties");
            UserLogin = (NguoiDungModel)Args[0];
            
            TenTaiKhoan = UserLogin.HoTen;
            LoaiTaiKhoan = UserLogin.TenLoai;
            //SCRC020Screen = "SCRC020V_DanhSachChung.xaml";
            //Loai nguoi dung
            //1: quan tri vien
            //2: quan ly
            //3: nhan vien
            if (UserLogin.LoaiNV == 1)
            {
                Tab1Header = "Danh Sách Người Dùng";
                Tab1FrameSource = "SCED010V_Nguoidung.xaml";
                Tab2Header = "Quy Định";
                Tab2FrameSource = "SCRC080V_QuyDinh.xaml";
                Tab3FrameSource = null;
            }
            else if (UserLogin.LoaiNV == 2) //Quanly
            {
                Tab1Header = "Thông Tin Giải Đấu";
                Tab1FrameSource = "SCRC060V_Thongtin.xaml";
                Tab2Header = "Thông Tin Cá Nhân";
                Tab2FrameSource = "SCED010V_Nguoidung.xaml";
                Tab3Header = "Danh Sách";
                Tab3FrameSource = "SCRC020V_DanhSachChung.xaml";

            }
            else if (UserLogin.LoaiNV == 3) // NhanVien
            {
                Tab1Header = "Thông Tin Giải Đấu";
                Tab1FrameSource = "SCRC060V_Thongtin.xaml";
                Tab2Header = "Thông Tin Cá Nhân";
                Tab2FrameSource = "SCED010V_Nguoidung.xaml";
                Tab3Header = "Danh Sách";
                Tab3FrameSource = "SCRC020V_DanhSachChung.xaml";
            }
        }
        #endregion

        #region Command
        void ExecuteLogoutCommand(object obj)
        {
        }

        void ExecuteCancelCommand(object obj)
        {
        }

        void ExecuteMenuTabChangeCommand(object obj)
        {
            //switch (MenuTabIndex)
            //{
            //    case 0: break;
            //    case 1:
            //        {
            //            SCRC020Screen = string.Empty;
            //            SCRC020Screen = "SCRC020V_DanhSachChung.xaml";
            //            InitializeProperties();
            //            break;
            //        }
            //    default:
            //        break;
            //}
        }

        #endregion

        #region Dao
        class Dao
        {
        }
        #endregion

        #region Class Variables
        string _tenTaiKhoan = "";
        public string TenTaiKhoan
        {
            get { return _tenTaiKhoan; }
            set
            {
                _tenTaiKhoan = value;
                RaisePropertyChanged("TenTaiKhoan");
            }
        }

        string _loaiTaiKhoan = "";
        public string LoaiTaiKhoan
        {
            get { return _loaiTaiKhoan; }
            set
            {
                _loaiTaiKhoan = value;
                RaisePropertyChanged("LoaiTaiKhoan");
            }
        }

        int _menuTabIndex = 0;

        public int MenuTabIndex
        {
            get { return _menuTabIndex; }
            set
            {
                _menuTabIndex = value;
                RaisePropertyChanged("MenuTabIndex");
            }
        }

        string _SCRC020Screen = null;

        //public string SCRC020Screen
        //{
        //    get { return _SCRC020Screen; }
        //    set
        //    {
        //        _SCRC020Screen = value;
        //        RaisePropertyChanged("SCRC020Screen");
        //    }

        //}

        NguoiDungModel _userLogin = new NguoiDungModel();
        public NguoiDungModel UserLogin
        {
            get { return _userLogin; }
            set
            {
                _userLogin = value;
                RaisePropertyChanged("UserLogin");
            }
        }

        string _Tab1Header = String.Empty;
        string _Tab2Header = String.Empty;
        string _Tab3Header = String.Empty;
        string _Tab1FrameSource = String.Empty;
        string _Tab2FrameSource = String.Empty;
        string _Tab3FrameSource = String.Empty;


        public string Tab1Header
        {
            get
            {
                return _Tab1Header;
            }

            set
            {
                _Tab1Header = value;
                RaisePropertyChanged("Tab1Header");
            }
        }

        public string Tab1FrameSource
        {
            get
            {
                return _Tab1FrameSource;
            }

            set
            {
                _Tab1FrameSource = value;
                RaisePropertyChanged("Tab1FrameSource");
            }
        }

        public string Tab2Header
        {
            get
            {
                return _Tab2Header;
            }

            set
            {
                _Tab2Header = value;
                RaisePropertyChanged("Tab2Header");
            }
        }

        public string Tab2FrameSource
        {
            get
            {
                return _Tab2FrameSource;
            }

            set
            {
                _Tab2FrameSource = value;
                RaisePropertyChanged("Tab2FrameSource");
            }
        }
        public string Tab3Header
        {
            get
            {
                return _Tab3Header;
            }

            set
            {
                _Tab3Header = value;
                RaisePropertyChanged("Tab3Header");
            }
        }

        public string Tab3FrameSource
        {
            get
            {
                return _Tab3FrameSource;
            }

            set
            {
                _Tab3FrameSource = value;
                RaisePropertyChanged("Tab3FrameSource");
            }
        }
        #endregion
    }
}
