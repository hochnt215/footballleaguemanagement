﻿//------------------------------------------------------------------------------
// <auto-generated>
//     text template column for binding
//
//     Auto refresh
// </auto-generated>
//------------------------------------------------------------------------------

using System.Windows.Input;
using Microsoft.TeamFoundation.MVVM;
using Utils;
namespace QuanLyGiaiBongDa.ViewModels
{

	partial class SCLG010VM_Login : BaseViewModel
	{

		private ICommand _loginCommand;

		public ICommand LoginCommand
		{
			get
			{
				if (_loginCommand == null)
				{
					_loginCommand = 
						new RelayCommand(ExecuteLoginCommand, CanExecuteLoginCommand);
				}
				return _loginCommand;
			}
		}

		private ICommand _cancelCommand;

		public ICommand CancelCommand
		{
			get
			{
				if (_cancelCommand == null)
				{
					_cancelCommand = new RelayCommand(ExecuteCancelCommand);
				}
				return _cancelCommand;
			}
		}

		private ICommand _openDoiMatKhauCommand;

		public ICommand OpenDoiMatKhauCommand
		{
			get
			{
				if (_openDoiMatKhauCommand == null)
				{
					_openDoiMatKhauCommand = new RelayCommand(ExecuteOpenDoiMatKhauCommand);
				}
				return _openDoiMatKhauCommand;
			}
		}

	}
}



