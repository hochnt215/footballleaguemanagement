﻿//------------------------------------------------------------------------------
// <auto-generated>
//     text template column for binding
//
//     Auto refresh
// </auto-generated>
//------------------------------------------------------------------------------

using System.Windows.Input;
using Microsoft.TeamFoundation.MVVM;
using Utils;
namespace QuanLyGiaiBongDa.ViewModels
{

	partial class SCED030VM_DangKyDoiBong : BaseViewModel
	{

		private ICommand _chooseLogoCommand;

		public ICommand ChooseLogoCommand
		{
			get
			{
				if (_chooseLogoCommand == null)
				{
					_chooseLogoCommand = new RelayCommand(ExecuteChooseLogoCommand);
				}
				return _chooseLogoCommand;
			}
		}

	}
}



