<#@ template debug="true" hostspecific="false" language="C#" #>
<#@ assembly name="System.Core" #>
<#@ import namespace="System.Linq" #>
<#@ import namespace="System.Text" #>
<#@ import namespace="System.Collections.Generic" #>
<#@ output extension=".cs" #>
<#@ include file="./CommandTemplate.txt" #>

<#+
	// Xac dinh ten mien NameSpace
	const string NamespaceName = "ViewModels";

	// Ten Class
	const string ClassName = "ExampleVM";

	// Xac dinh ten command va yeu cau check cho phep xu ly command
	private static string[,] Values = { { "Store", "true" }, { "Cancel", "false" } };
#>