﻿/*
 * 1642021 - Ha Nguyen Thai Hoc
 */
using QuanLyGiaiBongDa.ViewModels;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace QuanLyGiaiBongDa.Views
{
    /// <summary>
    /// Interaction logic for SCRC060V_Thongtin.xaml
    /// </summary>
    public partial class SCRC060V_Thongtin : UserControl
    {
        SCRC060VM_Thongtin vm;
        public SCRC060V_Thongtin()
        {
            InitializeComponent();
            //Application.Current.Resources["WindowColorStyle"] = (SolidColorBrush)(new BrushConverter().ConvertFrom(Common.Constants.WLIGHT_STYLE));
            //Background = (Brush)Application.Current.FindResource("WindowColorStyle");
            vm = new SCRC060VM_Thongtin();
            this.DataContext = vm;
        }

        private void BTN_LOGOUT_00_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            vm = (SCRC060VM_Thongtin)DataContext;
            if (vm.CloseAction == null)
                vm.CloseAction = new Action(() =>
            {
                var window = Window.GetWindow(this);
                if (window != null)
                {
                    window.Close();
                }
            });
        }
    }
}
