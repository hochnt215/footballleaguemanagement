﻿/*
 * 1642021 - Ha Nguyen Thai Hoc
 * 11/03/2017 - SCLG010V_Login
 */
using MahApps.Metro;
using MahApps.Metro.Controls;
using QuanLyGiaiBongDa.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace QuanLyGiaiBongDa.Views
{
    /// <summary>
    /// Interaction logic for SCLG010V_Login.xaml
    /// </summary>
    public partial class SCLG010V_Login : MetroWindow
    {
        public SCLG010V_Login()
        {
            InitializeComponent();
            // get the current app style (theme and accent) from the application
            // you can then use the current theme and custom accent instead set a new theme
            //Tuple<AppTheme, Accent> appStyle = ThemeManager.DetectAppStyle(Application.Current);

            // now set the Green accent and dark theme
            string themsStyle = Properties.Settings.Default.WindowColor;
            string btnColor = Properties.Settings.Default.ButtonColor;

            if ("BaseDark".Equals(themsStyle))
            {
                ThemeManager.ChangeAppStyle(Application.Current,
                                         ThemeManager.GetAccent(btnColor),
                                         ThemeManager.GetAppTheme("BaseDark")); // or appStyle.Item1
            }
            else
            {
                ThemeManager.ChangeAppStyle(Application.Current,
                                        ThemeManager.GetAccent(btnColor),
                                        ThemeManager.GetAppTheme("BaseLight")); // or appStyle.Item1

            }
            

            //string style = Properties.Settings.Default.WindowColor;
            //if ("DarkStyle".Equals(style))
            //{
            //    Application.Current.Resources["WindowColorStyle"] = (SolidColorBrush)(new BrushConverter().ConvertFrom(Common.Constants.WDARK_STYLE));
            //    //button and text content color
            //    Application.Current.Resources["ContentColorStyle"] = (SolidColorBrush)(new BrushConverter().ConvertFrom(Common.Constants.BASIC_LIGHT_COLOR));
            //}
            //else
            //{

            //    Application.Current.Resources["WindowColorStyle"] = (SolidColorBrush)(new BrushConverter().ConvertFrom(Common.Constants.WLIGHT_STYLE));
            //    //button and text content color
            //    Application.Current.Resources["ContentColorStyle"] = (SolidColorBrush)(new BrushConverter().ConvertFrom(Common.Constants.BASIC_DARK_COLOR));
            //}
            //Background = (Brush)Application.Current.FindResource("WindowColorStyle");
            //btnDoiMatKhau.BorderBrush = Colors.Black;
        }
    }
}
