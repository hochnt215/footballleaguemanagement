﻿using QuanLyGiaiBongDa.ViewModels;
using System.Windows;
/*
* 1642021 - Ha Nguyen Thai Hoc
* 23/03/2017 - SCRC020V_DanhSachChung.xaml
*/
using System.Windows.Controls;
using System.Windows.Media;

namespace QuanLyGiaiBongDa.Views
{
    /// <summary>
    /// Interaction logic for SCRC020V_DanhSachChung.xaml
    /// </summary>
    public partial class SCRC020V_DanhSachChung : UserControl
    {
        SCRC020VM_DanhSachChung vm;
        public SCRC020V_DanhSachChung()
        {
            InitializeComponent();
            //Background = (Brush)Application.Current.MainWindow.FindResource("WindowColorStyle");
            vm = new SCRC020VM_DanhSachChung();
            this.DataContext = vm;
        }

	}
}
