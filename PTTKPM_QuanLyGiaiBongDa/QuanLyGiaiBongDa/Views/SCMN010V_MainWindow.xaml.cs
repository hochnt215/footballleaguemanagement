﻿/*
 * 1642021 - Ha Nguyen Thai Hoc
 * 12/03/2017 - SCMN010V_MainWindow
 */
using MahApps.Metro;
using MahApps.Metro.Controls;
using System.Windows;
using System.Windows.Media;

namespace QuanLyGiaiBongDa.Views
{
    /// <summary>
    /// Interaction logic for SCMN010V_MainWindow.xaml
    /// </summary>
    public partial class SCMN010V_MainWindow : MetroWindow
    {
        string _colorTheme = string.Empty;
        string _colorButton = string.Empty;
        public SCMN010V_MainWindow()
        {
            InitializeComponent();
            //Background = (Brush)Application.Current.MainWindow.FindResource("WindowColorStyle");
            _colorTheme = Properties.Settings.Default.WindowColor;
            _colorButton = Properties.Settings.Default.ButtonColor;

        }

        private void btnSetDarkSkin_Click(object sender, RoutedEventArgs e)
        {
            if(MessageBox.Show("Bạn có chắc chắn muốn chuyển sang giao diện tối không?","",MessageBoxButton.YesNo) == MessageBoxResult.No)
                return;
            EditSkinWindow("BaseDark");
        }

        private void btnSetLightSkin_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Bạn có chắc chắn muốn chuyển sang giao diện tối không?", "", MessageBoxButton.YesNo) == MessageBoxResult.No)
                return;
            EditSkinWindow("BaseLight");
        }

        private void btnColorSteel_Click(object sender, RoutedEventArgs e)
        {
            EditButtonStyle("Steel");
        }

        private void btnColorBlue_Click(object sender, RoutedEventArgs e)
        {
            EditButtonStyle("Blue");
        }

        private void btnColorOlive_Click(object sender, RoutedEventArgs e)
        {
            EditButtonStyle("Olive");
        }

        private void btnColorRed_Click(object sender, RoutedEventArgs e)
        {
            EditButtonStyle("Red");
        }

        private void btnColorCyan_Click(object sender, RoutedEventArgs e)
        {
            EditButtonStyle("Cyan");
        }

        private void EditSkinWindow(string wColor)
        {
            Properties.Settings.Default.WindowColor = wColor;
            Properties.Settings.Default.Save();
            ThemeManager.ChangeAppStyle(Application.Current,
                                         ThemeManager.GetAccent(_colorButton),
                                         ThemeManager.GetAppTheme(_colorTheme = wColor)); // or appStyle.Item1
        }

        private void EditButtonStyle(string bColor)
        {
            Properties.Settings.Default.ButtonColor = bColor;
            Properties.Settings.Default.Save();
            ThemeManager.ChangeAppStyle(Application.Current,
                                         ThemeManager.GetAccent(_colorButton = bColor),
                                         ThemeManager.GetAppTheme(_colorTheme)); // or appStyle.Item1
        }


    }
}
