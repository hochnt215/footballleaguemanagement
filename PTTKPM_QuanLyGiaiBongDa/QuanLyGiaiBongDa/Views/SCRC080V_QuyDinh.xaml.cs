﻿using System;
using System.Windows;
using System.Windows.Controls;
using QuanLyGiaiBongDa.ViewModels;

namespace QuanLyGiaiBongDa.Views
{
    /// <summary>
    /// Interaction logic for SCRC080V_QuyDinh.xaml
    /// </summary>
    public partial class SCRC080V_QuyDinh : UserControl
    {
        SCRC080VM_QuyDinh vm;
        public SCRC080V_QuyDinh()
        {
            InitializeComponent();
            vm = new SCRC080VM_QuyDinh();
            this.DataContext = vm;
        }

        private void BTN_LOGOUT_00_Click(object sender, RoutedEventArgs e)
        {
            vm = (SCRC080VM_QuyDinh)DataContext;
            if (vm.CloseAction == null)
                vm.CloseAction = new Action(() =>
                {
                    var window = Window.GetWindow(this);
                    if (window != null)
                    {
                        window.Close();
                    }
                });
        }
    }
}
